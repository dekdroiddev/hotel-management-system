class AppLink {
  static const String HOME = "/home";
  static const String CHECKIN = "/checkin";
  static const String CHECKOUT = "/checkout";
  static const String MANAGE = "/management";
  static const String INFO = "/information";
  static const String CUSINFO = "/customerinfo";
}
