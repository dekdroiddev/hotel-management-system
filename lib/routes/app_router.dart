import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/binding/checkinBinding.dart';
import 'package:hotel_management/feature/checkin/checkinScreen.dart';
import 'package:hotel_management/feature/checkout/binding/checkoutBinding.dart';
import 'package:hotel_management/feature/checkout/checkoutScreen.dart';
import 'package:hotel_management/feature/home/binding/homeBinding.dart';
import 'package:hotel_management/feature/home/myHomeScreen.dart';
import 'package:hotel_management/feature/information/binding/informationBinding.dart';
import 'package:hotel_management/feature/information/customerInfoScreen.dart';
import 'package:hotel_management/feature/information/informationScreen.dart';
import 'package:hotel_management/feature/manage/binding/manageBinding.dart';
import 'package:hotel_management/feature/manage/manageScreen.dart';
import 'package:hotel_management/routes/app_link.dart';

class AppRouter {
  static final pages = [
    GetPage(
        name: AppLink.HOME,
        page: () => const MyHomeScreen(
              title: '',
            ),
        binding: HomeBinding()),
    GetPage(
        name: AppLink.CHECKIN,
        page: () => const CheckinScreen(
              title: 'Check In',
            ),
        binding: CheckinBinding()),
    GetPage(
        name: AppLink.CHECKOUT,
        page: () => const CheckoutScreen(
              title: 'Check Out',
            ),
        binding: CheckoutBinding()),
    GetPage(
        name: AppLink.MANAGE,
        page: () => const ManageScreen(
              title: 'Hotel Meanagement',
            ),
        binding: ManageBinding()),
    GetPage(
        name: AppLink.INFO,
        page: () => const InformationScreen(
              title: 'Hotel info',
            ),
        binding: InformationBinding()),
    GetPage(
        name: AppLink.CUSINFO,
        page: () => const CustomerInfoScreen(
              title: 'Customer info',
            ),
        binding: InformationBinding()),
  ];
}
