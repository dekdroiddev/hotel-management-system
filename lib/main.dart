import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/controller/checkinController.dart';
import 'package:hotel_management/feature/checkout/controller/checkOutController.dart';
import 'package:hotel_management/feature/home/controller/homeController.dart';
import 'package:hotel_management/feature/home/myHomeScreen.dart';
import 'package:hotel_management/feature/manage/controller/manageController.dart';
import 'package:hotel_management/feature/manage/manageScreen.dart';

import 'feature/checkin/checkinScreen.dart';
import 'routes/app_router.dart';

final navigatorKey = GlobalKey<NavigatorState>();
void main() {
  initServices();
  runApp(const MyApp());
}

initServices() async {
  WidgetsFlutterBinding.ensureInitialized();
  Get.lazyPut<ManageController>(() => ManageController());
  Get.lazyPut<CheckinController>(() => CheckinController());
  Get.lazyPut<HomeController>(() => HomeController());
  Get.lazyPut<CheckoutController>(() => CheckoutController());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      navigatorKey: navigatorKey,
      getPages: AppRouter.pages,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      home: const MyHomeScreen(title: 'Flutter Demo Home Page'),
    );
  }
}
