import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/controller/checkinController.dart';
import 'package:hotel_management/feature/manage/controller/manageController.dart';
import 'package:hotel_management/routes/app_link.dart';

class MyHomeScreen extends StatefulWidget {
  const MyHomeScreen({super.key, required this.title});

  final String title;

  @override
  State<MyHomeScreen> createState() => _MyHomeScreenState();
}

class _MyHomeScreenState extends State<MyHomeScreen> {
  ManageController manageController = Get.find();
  CheckinController checkinController = Get.find();

  @override
  void initState() {
    super.initState();
    // hospitalController.onReqestHospital();
  }

  @override
  Widget build(BuildContext context) {
    Widget boxDelivery(String textTitle, IconData? icon) {
      return Card(
        elevation: 3,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 20.0, right: 20, bottom: 20, top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(icon, color: Colors.black),
              const SizedBox(
                width: 10,
              ),
              Text(textTitle)
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(15),
        color: Colors.blueGrey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
              height: 50,
            ),
            InkWell(
                onTap: () {
                  if (manageController.hotalList.value.isEmpty) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('please create hotel')));
                  } else {
                    Get.toNamed(AppLink.CHECKIN);
                  }
                },
                child: boxDelivery("Check in", Icons.input_rounded)),
            const SizedBox(height: 10),
            InkWell(
                onTap: () {
                  if (checkinController.customerList.value.isEmpty) {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('not have customer check in')));
                  } else {
                    Get.toNamed(AppLink.CHECKOUT);
                  }
                },
                child: boxDelivery("Check out", Icons.outbond_outlined)),
            Container(
              margin: const EdgeInsets.only(
                  top: 20, bottom: 20, left: 20, right: 20),
              height: 1,
              color: Colors.black12,
            ),
            InkWell(
                onTap: () {
                  Get.toNamed(AppLink.MANAGE);
                },
                child: boxDelivery(
                    "Hotel Management", Icons.add_business_outlined)),
          ],
        ),
      ),
    );
  }
}
