import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/controller/checkinController.dart';
import 'package:hotel_management/feature/checkout/controller/checkOutController.dart';
import 'package:hotel_management/feature/manage/controller/manageController.dart';

import '../controller/homeController.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ManageController>(() => ManageController());
    Get.lazyPut<CheckinController>(() => CheckinController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<CheckoutController>(() => CheckoutController());
  }
}
