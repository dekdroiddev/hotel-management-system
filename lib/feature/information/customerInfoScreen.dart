import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/model/customerModel.dart';
import 'package:hotel_management/feature/manage/model/hotelModel.dart';

class CustomerInfoScreen extends StatefulWidget {
  const CustomerInfoScreen({super.key, required this.title});

  final String title;

  @override
  State<CustomerInfoScreen> createState() => _CustomerInfoScreenState();
}

class _CustomerInfoScreenState extends State<CustomerInfoScreen> {
//  var listData;
  var checkListData;

  List<CustomerModel>? customerList = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var argumentsData = Get.arguments;
    checkListData = argumentsData[0] as String;
    customerList = argumentsData[1] as List<CustomerModel>;

    Widget boxDelivery(CustomerModel customerModel) {
      return Card(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Icon(Icons.hotel, color: Colors.black),
              const SizedBox(
                width: 10,
              ),
              Text(
                  'Room ${customerModel.room} is booked by ${customerModel.name} \nwith keycard number ${customerModel.keycardNumber}')
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Container(
          padding: const EdgeInsets.all(5),
          color: const Color(0xFF8ec153),
          child: Visibility(
            visible: customerList!.isEmpty ? false : true,
            child: ListView.builder(
                itemCount: customerList!.length,
                itemBuilder: (context, index) {
                  return boxDelivery(customerList![index]);
                }),
          )),
    );
  }
}
