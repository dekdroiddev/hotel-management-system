import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/model/customerModel.dart';
import 'package:hotel_management/feature/manage/model/hotelModel.dart';

class InformationScreen extends StatefulWidget {
  const InformationScreen({super.key, required this.title});

  final String title;

  @override
  State<InformationScreen> createState() => _InformationScreenState();
}

class _InformationScreenState extends State<InformationScreen> {
//  var listData;
  var checkListData;
  List<HotelModel>? hotelList = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var argumentsData = Get.arguments;
    checkListData = argumentsData[0] as String;

    if (checkListData == '1') {
      hotelList = argumentsData[1] as List<HotelModel>;
    }

    Widget boxDelivery(HotelModel hotelModel) {
      return Card(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Icon(Icons.hotel, color: Colors.black),
              const SizedBox(
                width: 10,
              ),
              Text('${hotelModel.floor!},  room ${hotelModel.room!}')
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Container(
          padding: const EdgeInsets.all(5),
          color: const Color(0xFF8ec153),
          child: Visibility(
            visible: hotelList!.isEmpty ? false : true,
            child: ListView.builder(
                itemCount: hotelList!.length,
                itemBuilder: (context, index) {
                  return boxDelivery(hotelList![index]);
                }),
          )),
    );
  }
}
