import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/model/customerModel.dart';
import 'package:hotel_management/feature/manage/model/hotelModel.dart';

class CheckinController extends GetxController {
  final customerList = <CustomerModel>[].obs;

  @override
  void onInit() {
    super.onInit();
  }

  onUserCheckin(String name, int age, int room) async {
    customerList.add(
        CustomerModel(name: name, age: age, room: room, keycardNumber: room));
  }

  bool onAviableRoom(List<HotelModel> list, String room) {
    for (var i = 0; i < list.length; i++) {
      if (list[i].room.toString().contains(room)) {
        log(room);

        return true;
      }
    }
    return false;
  }
}
