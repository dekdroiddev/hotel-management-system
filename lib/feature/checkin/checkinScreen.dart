import 'dart:developer' as lg;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/controller/checkinController.dart';
import 'package:hotel_management/feature/manage/controller/manageController.dart';

class CheckinScreen extends StatefulWidget {
  const CheckinScreen({super.key, required this.title});

  final String title;

  @override
  State<CheckinScreen> createState() => _CheckinScreenState();
}

class _CheckinScreenState extends State<CheckinScreen> {
  CheckinController checkinController = Get.find();
  ManageController manageController = Get.find();

  final TextEditingController nameFieldController = TextEditingController();
  final TextEditingController ageFieldController = TextEditingController();
  final TextEditingController roomFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  Future<void> _displayTextInputDialog(BuildContext context, String name,
      String age, String room, String keycard) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Check in Success!',
                style: TextStyle(color: Colors.black, fontSize: 16)),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Name : $name'),
                Text('Age : $age'),
                Text('Room : $room'),
                Text('Keycard number : $keycard')
              ],
            ),
            actions: <Widget>[
              MaterialButton(
                color: Colors.green,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  nameFieldController.text = '';
                  ageFieldController.text = '';
                  roomFieldController.text = '';

                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    isRoomCanCheckin() {
      bool isCheckin = true;

      for (var i = 0; i < checkinController.customerList.length; i++) {
        int room = checkinController.customerList[i].room!;
        if (room == int.parse(roomFieldController.text)) {
          isCheckin = false;
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                  'Cannot book room ${roomFieldController.text} for ${nameFieldController.text}, The room is currently booked by ${checkinController.customerList[i].name}.')));
        }
      }

      if (isCheckin) {
        checkinController.onUserCheckin(
            nameFieldController.text,
            int.parse(ageFieldController.text),
            int.parse(roomFieldController.text));

        _displayTextInputDialog(
            context,
            nameFieldController.text,
            ageFieldController.text,
            roomFieldController.text,
            roomFieldController.text);
      }
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextFormField(
                controller: nameFieldController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter name',
                ),
              ),
            ),
            const SizedBox(height: 4),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: TextFormField(
                controller: ageFieldController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter age',
                ),
                keyboardType: TextInputType.number,
                maxLength: 3,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
              ),
            ),
            const SizedBox(height: 4),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: TextFormField(
                controller: roomFieldController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter  booking room number',
                ),
                keyboardType: TextInputType.number,
                maxLength: 3,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20),
              child: Center(
                child: ElevatedButton(
                  style: const ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll<Color>(Colors.lightGreen),
                      padding: MaterialStatePropertyAll<EdgeInsetsGeometry>(
                          EdgeInsets.symmetric(vertical: 10, horizontal: 20))),
                  onPressed: () {
                    if (nameFieldController.text.isEmpty ||
                        ageFieldController.text.isEmpty ||
                        roomFieldController.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text('please input all field')));
                    } else {
                      manageController.generateHotel(
                          int.parse(manageController.floorCodeDialog.value),
                          int.parse(manageController.roomCodeDialog.value));

                      bool room = checkinController.onAviableRoom(
                          manageController.hotalList, roomFieldController.text);

                      if (room) {
                        isRoomCanCheckin();
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content:
                                    Text('this room number not available')));
                      }
                    }
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8),
                    child: Text(
                      style: TextStyle(color: Colors.black),
                      'check in',
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
