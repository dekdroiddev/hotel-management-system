class CustomerModel {
  String? name;
  int? age;
  int? room;
  int? keycardNumber;

  CustomerModel(
      {required this.name,
      required this.age,
      required this.room,
      required this.keycardNumber});

  get getName => this.name;

  set setName(name) => this.name = name;

  get getAge => this.age;

  set setAge(age) => this.age = age;

  get getRoom => this.room;

  set setRoom(room) => this.room = room;

  get getKeycardNumber => this.keycardNumber;

  set setKeycardNumber(keycardNumber) => this.keycardNumber = keycardNumber;
}
