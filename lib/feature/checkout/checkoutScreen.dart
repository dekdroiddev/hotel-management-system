import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/controller/checkinController.dart';
import 'package:hotel_management/feature/manage/controller/manageController.dart';

class CheckoutScreen extends StatefulWidget {
  const CheckoutScreen({super.key, required this.title});

  final String title;

  @override
  State<CheckoutScreen> createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  CheckinController checkinController = Get.find();
  ManageController manageController = Get.find();

  final TextEditingController nameFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
    // hospitalController.onReqestHospital();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> _displayTextInputDialog(BuildContext context, String name,
        String age, String room, String keycard) async {
      return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('Check out Success!',
                  style: TextStyle(color: Colors.black, fontSize: 16)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Name : $name'),
                  Text('Age : $age'),
                  Text('Room : $room'),
                  Text('Keycard number : $keycard')
                ],
              ),
              actions: <Widget>[
                MaterialButton(
                  color: Colors.green,
                  textColor: Colors.white,
                  child: const Text('OK'),
                  onPressed: () {
                    nameFieldController.text = '';

                    Navigator.pop(context);
                  },
                ),
              ],
            );
          });
    }

    onRemoveCheckin(BuildContext context) {
      for (var i = 0; i < checkinController.customerList.length; i++) {
        if (checkinController.customerList[i].name!
            .contains(nameFieldController.text)) {
          _displayTextInputDialog(
              context,
              checkinController.customerList[i].name!,
              checkinController.customerList[i].age.toString(),
              checkinController.customerList[i].room.toString(),
              checkinController.customerList[i].keycardNumber.toString());
          checkinController.customerList.removeAt(i);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('can\'t find your room')));
        }
      }
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextFormField(
                controller: nameFieldController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter name',
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20),
              child: Center(
                child: ElevatedButton(
                  style: const ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll<Color>(Colors.lightGreen),
                      padding: MaterialStatePropertyAll<EdgeInsetsGeometry>(
                          EdgeInsets.symmetric(vertical: 10, horizontal: 20))),
                  onPressed: () {
                    onRemoveCheckin(context);
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8),
                    child: Text(
                      style: TextStyle(color: Colors.black),
                      'check out',
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
