import 'dart:developer';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/controller/checkinController.dart';
import 'package:hotel_management/feature/checkin/model/customerModel.dart';
import 'package:hotel_management/feature/checkout/controller/checkOutController.dart';
import 'package:hotel_management/feature/home/controller/homeController.dart';
import 'package:hotel_management/feature/manage/controller/manageController.dart';
import 'package:hotel_management/routes/app_link.dart';

class ManageScreen extends StatefulWidget {
  const ManageScreen({super.key, required this.title});

  final String title;

  @override
  State<ManageScreen> createState() => _ManageScreenState();
}

class _ManageScreenState extends State<ManageScreen> {
  ManageController manageController = Get.find();
  CheckinController checkinController = Get.find();
  CheckoutController checkoutController = Get.find();

  RangeValues values = const RangeValues(1, 100);
  RangeLabels labels = const RangeLabels('1', "100");
  RangeValues _currentRangeValues = const RangeValues(40, 80);

  final TextEditingController floorFieldController = TextEditingController();
  final TextEditingController roomFieldController = TextEditingController();

  final TextEditingController roomSearchController = TextEditingController();

  String? floorValueText = '0';
  String? roomValueText = '0';

  String? searchValueText = '';

  var listData;

  @override
  void initState() {
    super.initState();
  }

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  onChanged: (value) {
                    setState(() {
                      floorValueText = value;
                    });
                  },
                  controller: floorFieldController,
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: 'Enter Floor',
                  ),
                  maxLength: 1,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                TextFormField(
                  onChanged: (value) {
                    setState(() {
                      roomValueText = value;
                    });
                  },
                  controller: roomFieldController,
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: 'Enter Room',
                  ),
                  keyboardType: TextInputType.number,
                  maxLength: 2,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                ),
              ],
            ),
            actions: <Widget>[
              MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                child: const Text('CANCEL'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              MaterialButton(
                color: Colors.green,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  setState(() {
                    manageController.floorCodeDialog.value = floorValueText!;
                    manageController.roomCodeDialog.value = roomValueText!;
                    manageController.generateHotel(
                        int.parse(manageController.floorCodeDialog.value),
                        int.parse(manageController.roomCodeDialog.value));

                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }

  Future<void> _roomSearchTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  onChanged: (value) {
                    setState(() {
                      searchValueText = value;
                    });
                  },
                  controller: roomSearchController,
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: 'Enter room number',
                  ),
                  maxLength: 3,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                ),
              ],
            ),
            actions: <Widget>[
              MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                child: const Text('CANCEL'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              MaterialButton(
                color: Colors.green,
                textColor: Colors.white,
                child: const Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                  genCustomerByroom();
                },
              ),
            ],
          );
        });
  }

  Future<void> _displayRangeAgeDialog(BuildContext context) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, innerSetState) {
          return AlertDialog(
              title: const Text(
                "adjust age range",
                style: TextStyle(fontSize: 18),
              ),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  RangeSlider(
                    values: _currentRangeValues,
                    min: 1,
                    max: 120,
                    divisions: 119,
                    labels: RangeLabels(
                      _currentRangeValues.start.round().toString(),
                      _currentRangeValues.end.round().toString(),
                    ),
                    onChanged: (RangeValues values) {
                      innerSetState(() {
                        _currentRangeValues = values;
                      });
                    },
                  )
                ],
              ),
              actions: <Widget>[
                MaterialButton(
                  color: Colors.red,
                  textColor: Colors.white,
                  child: const Text('CANCEL'),
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                ),
                MaterialButton(
                  color: Colors.green,
                  textColor: Colors.white,
                  child: const Text('OK'),
                  onPressed: () {
                    setState(() {
                      log("${_currentRangeValues.start.round()}\n${_currentRangeValues.end.round()}");

                      Navigator.pop(context);
                    });
                    genCustomerByAge(_currentRangeValues.start.round(),
                        _currentRangeValues.end.round());
                  },
                ),
              ]);
        });
      },
    );
  }

  genAviableRoom() {
    manageController.generateHotel(
        int.parse(manageController.floorCodeDialog.value),
        int.parse(manageController.roomCodeDialog.value));

    if (checkinController.customerList.isEmpty) {
      listData = manageController.hotalList;
    } else {
      manageController.availableRoomList.value = manageController.hotalList;
      log(manageController.hotalList.length.toString());
      for (var i = 0; i < checkinController.customerList.length; i++) {
        log(checkinController.customerList[i].room.toString());
        for (var j = 0; j < manageController.hotalList.length; j++) {
          if (checkinController.customerList[i].room
              .toString()
              .contains(manageController.hotalList[j].room!)) {
            manageController.availableRoomList
                .remove(manageController.hotalList[j]);
          }
        }
      }
      listData = manageController.availableRoomList;
    }

    if (listData != null) {
      Get.toNamed(AppLink.INFO, arguments: ['1', listData]);
    }
  }

  genAllCustomer() {
    listData = checkinController.customerList;
    // log(checkinController.customerList[0].keycardNumber.toString());

    if (listData != null) {
      Get.toNamed(AppLink.CUSINFO, arguments: ['2', listData]);
    }
  }

  genCustomerByAge(int start, int end) {
    List<CustomerModel> cus = [];

    for (var i = 0; i < checkinController.customerList.length; i++) {
      int age = checkinController.customerList[i].age!;
      if (start < age && age < end) {
        cus.add(checkinController.customerList[i]);
      }
    }

    listData = cus;
    if (cus.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('can\'t have customer in this age range')));
    } else {
      if (listData != null) {
        Get.toNamed(AppLink.CUSINFO, arguments: ['3', listData]);
      }
    }
  }

  genCustomerByroom() {
    List<CustomerModel> cus = [];
    for (var i = 0; i < checkinController.customerList.length; i++) {
      int room = checkinController.customerList[i].room!;
      if (room == int.parse(roomSearchController.text)) {
        cus.add(checkinController.customerList[i]);
      }
    }

    listData = cus;
    if (cus.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('can\'t have customer in this room')));
    } else {
      if (listData != null) {
        roomSearchController.text = '';
        Get.toNamed(AppLink.CUSINFO, arguments: ['3', listData]);
      }
    }

    // if (listData != null) {
    //   Get.toNamed(AppLink.CUSINFO, arguments: ['4', listData]);
    // }
  }

  @override
  Widget build(BuildContext context) {
    Widget boxDelivery(String textTitle, IconData? icon) {
      return Card(
        elevation: 3,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(icon, color: Colors.black),
              const SizedBox(
                width: 10,
              ),
              Text(textTitle)
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(15),
        color: Colors.blueGrey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 10),
              child: Obx(() => Text(
                    'Hotel created with \n${manageController.floorCodeDialog} floor(s), ${manageController.roomCodeDialog} room(s) per floor',
                    style: const TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  )),
            ),
            InkWell(
                onTap: () {
                  _displayTextInputDialog(context);
                },
                child: boxDelivery(
                    "Create Floor/Room", Icons.add_business_outlined)),
            const SizedBox(height: 10),
            InkWell(
                onTap: () {
                  genAviableRoom();
                },
                child: boxDelivery(
                    "Check available room", Icons.meeting_room_rounded)),
            const SizedBox(height: 10),
            InkWell(
                onTap: () {
                  genAllCustomer();
                },
                child: boxDelivery(
                    "Check all customer", Icons.people_alt_outlined)),
            const SizedBox(height: 10),
            InkWell(
                onTap: () {
                  _displayRangeAgeDialog(context);
                },
                child:
                    boxDelivery("Check customer by age", Icons.accessibility)),
            const SizedBox(height: 10),
            InkWell(
                onTap: () {
                  // genCustomerByroom();

                  _roomSearchTextInputDialog(context);
                },
                child: boxDelivery(
                    "Check customer by room", Icons.roofing_outlined)),
          ],
        ),
      ),
    );
  }
}
