class HotelModel {
  String? room;
  String? floor;

  HotelModel({required this.room, required this.floor});

  String? get getRoom => this.room;

  set setRoom(String? room) => this.room = room;

  get getFloor => this.floor;

  set setFloor(floor) => this.floor = floor;
}
