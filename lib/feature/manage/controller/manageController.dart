import 'dart:developer';
import 'dart:io';

import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/model/customerModel.dart';
import 'package:hotel_management/feature/manage/model/hotelModel.dart';

class ManageController extends GetxController {
  final hotalList = <HotelModel>[].obs;
  final availableRoomList = <HotelModel>[].obs;

  final customerByAgeList = <CustomerModel>[].obs;
  final customerByRoom = <CustomerModel>[].obs;

  final roomCodeDialog = '0'.obs;
  final floorCodeDialog = '0'.obs;

  generateHotel(int floor, int room) async {
    hotalList.clear();
    for (var i = 1; i <= floor; i++) {
      for (var j = 1; j <= room; j++) {
        String indexRoom = j.toString();
        if (indexRoom.length == 1) {
          indexRoom = "0$indexRoom";
        }
        hotalList.add(HotelModel(floor: '$i', room: '$i$indexRoom'));
        log('floor: $i, room: $i$indexRoom');
      }
    }
  }
}
