import 'package:get/get.dart';
import 'package:hotel_management/feature/checkin/controller/checkinController.dart';
import 'package:hotel_management/feature/checkout/controller/checkOutController.dart';
import 'package:hotel_management/feature/home/controller/homeController.dart';

import '../controller/manageController.dart';

class ManageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ManageController>(() => ManageController());
    Get.lazyPut<CheckinController>(() => CheckinController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<CheckoutController>(() => CheckoutController());
  }
}
